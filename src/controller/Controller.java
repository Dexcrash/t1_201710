package controller;

import java.util.ArrayList;

import model.data_structures.NumbersBag;
import model.logic.NumbersBagOperations;

public class Controller {

	private static NumbersBagOperations model = new NumbersBagOperations();
	
	public static NumbersBag createBag(ArrayList<Number> values){
         return new NumbersBag(values);		
	}
	
	public static double getMean(NumbersBag bag){
		return model.computeMean(bag);
	}
	
	public static double getMax(NumbersBag bag){
		return model.getMax(bag);
	}
	
	public static double getMin(NumbersBag bag){
		return model.getMin(bag);
	}
	
	public static double getTotalSum(NumbersBag bag){
		return model.getSumAll(bag);
	}
	
	public static double getSize(NumbersBag bag){
		return model.getSize(bag);
	}
	
	public static double getMedian(NumbersBag bag){
		return model.getMedian(bag);
	}

	
}
