package model.logic;

import java.util.ArrayList;
import java.util.Iterator;

import model.data_structures.NumbersBag;

public class NumbersBagOperations{
	
	public double getMax(NumbersBag bag){
		double max = Double.MIN_VALUE;
	    double value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}	
		}
		return max;
	}
	
	public double getMin(NumbersBag bag){
		double min = Integer.MAX_VALUE;
	    double value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
		}
		return min;
	}
	
	public double getSumAll(NumbersBag bag){
		double sum = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				sum += iter.next();
			}
		}
		return sum;
	}
	
	public int getSize(NumbersBag bag){
	    int tam =0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				iter.next();
				tam++;
			}
		}
		return tam;
	}
	
	public double computeMean(NumbersBag<Integer> bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	public double getMedian(NumbersBag bag){
		double med = 0;
	    int tam = getSize(bag);
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			int a = tam;
			while(a >= ((tam/2)+1)){
				med = iter.next();
				a--;
			}
		}
		return med;
	}

	
	
	
}
